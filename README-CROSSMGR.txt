CrossMgr Client                                                 Stuart Lynne
Wimsey                                          Fri Mar 18 10:25:42 PDT 2016 


CrossMgr client laptops are set up with the following:

    - latest copy of CrossMgr (with batch publish)
    - Win32 Apache
    - Cygwin with make, git, rsync and ssh
   


Apache2.4 is installed in c:/Apache24. 

Cygwin is installed in 
    
    c:/cygwin
    ln -s /cygdrive/c/Apache24/htdocs /var/htdocs



CrossMgr client support routines:


    crossmgrsync - copy result html files to local /var/htdocs/20NN
    crossmgrpush - push /var/htdocs/20nn to web site


Currently there are two web servers to push data to. 

    - aws1.wimsey.co 
    - whiskey.wimsey.co"

Both have a crossmgr user that can be accessed using rsync:

    h=aws1.wimsey.co
    rsync  --chmod=D2775,F664 -azv -e 'ssh -l sl' * crossmgr@${h}:~crossmgr/doc_root/

The aws1.wimsey.co site is available publicly as:
    
    https://crossmgr.wimsey.co/




Apache2.4
*********

After copying in the Apache2.4 directory to c:Apache24 start an administrator command shell.


    1. click start
    2. type cmd
    3. <shift><ctrl><enter>
    4. cd c:Apache24\bin
    5. httpd -k install -n Apache2.4

N.B Will auto-start on reboot, or use Services.msc to manually start for first test.

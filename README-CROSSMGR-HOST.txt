CrossMgr Results                                                Stuart Lynne
Wimsey                                          Fri Nov 14 16:20:47 PST 2014 


The crossmgr_support archive contains support for uploading CrossMgr results
to an ftp site and then make available via a website.


Overview
********

    doc_root/:
        index.cgi
        vcxc2014/
            2014-10-19-Atomic Superprestige-r1-.html
        evss2014/


The index.cgi script displays a sanitized view of the directory (removing
extraneous files.)

Clicking on a sub-directory will descend into the directory.

Clicking on a result file will display the results.


Perl Setup
**********

Some additional modules may need to be installed.

    cpan Path::Trim 

N.B. If running cpan as the local user the modules will be installed only for 
that user. E.g.:

    /home/username/perl5/lib/perl5

In this case the index.cgi script will need to be modified to have the
appropriate line uncommented and set for the correct directory name:

E.g.:
    use lib "/home/lynnerya/perl5/lib/perl5";




DNS Setup
*********

Create a DNS record with a CNAME pointing at the hosted domain:

    crossmgr.wimsey.co CNAME www.insighttrader.com


WWW Setup
*********

In the home directory of the user of the hosted server:

    git clone git@bitbucket.wimsey.co:stuartlynne/crossmgr_support

In cpanel create an addon domain.

    New domain name:            crossmgr.wimsey.co
    Subdomain/FTP Username:     crossmgr
    Password:
    Password (again):
    Document root $(HOME)/:     crossmgr_support/doc_root

This will create the web site for crossmgr.wimsey.co and will allow
access to user crossmgr@crossmgr.wimsey.co with the supplied password.
    

FTP Setup
*********

The FTP account is created with the WWW site.

For use with CrossMgr results are generally grouped into separate top level
directories. That can be by club, series, organizer etc.

For example:

    crossmgr_support/doc_root
        vcxc2014
        evss2014

These can be created via SSH access to the hosted site.

Alternately by using an ftp client:

    ftp crossmgr.wimsey.co
    Name: crossmgr@crossmgr.wimsey.co
    Password: XXXXXX
    dir
    mkdir vcxc2014
    dir
    quit


CrossMgr Setup
**************

Use the CrossMgr / Publish / FTP HTML Publish command and fill in the
following fields:

    FTP Host Name:                  crossmgr.wimsey.co
    Path on Host to Write HTML:     vcxc2014
    Path on Host to Write Photos:   photos
    User:                           crossmgr@bvc.wimsey.co
    Password:

    URL Path:                       http://crossmgr.wimsey.co/vcxc2014


End User Access
***************

Simply publish the website URL:

    http://crossmgr.wimsey.co

Or for a specific series:

    http://crossmgr.wimsey.co/vcxc2014


Simple Blogger Integration
**************************

These results can be simply integrated into another website, e.g. blogger by
using an iframe by using an HTML snippet like the following:

    <iframe frameborder="0" height="600" scrolling="no"
        src="http://crossmgr.wimsey.co/?path=./vcxc2014"
        style="border-width: 0;" width="800">
    </iframe>








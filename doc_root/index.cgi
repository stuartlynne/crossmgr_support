#!/usr/bin/perl -T

use strict;
use warnings;

# IFF you have installed local modules using cpan as the end user
# then uncomment the following line using the correct home directory
# 
use lib "/home/lynnerya/perl5/lib/perl5";
#



use CGI;
use File::Basename;
use File::Spec;

# N.B. - See note above, this will need to be installed and may
# cause problems if installed by the local user.
#
use Path::Trim;

use Data::Dumper;

my $cgi = CGI->new();

if ( my $file = $cgi->param('file') ) {
    open my $fh, '<', $file or die $!;

    print $cgi->header(
        '-type'           => 'application/octet-stream',
        '-attachment'     => basename($file),
        '-Content_Length' => -s $file,
    );

    binmode $fh;
    print while <$fh>;
}
else {
    #printf STDERR "ENV: %s\n", Dumper(\%ENV);
    #printf STDERR "CGI: %s\n", Dumper($cgi);

    my $doc_root = $ENV{'DOCUMENT_ROOT'};                                                                
    my $path = "";

    $path = $cgi->param('path') if (defined($cgi->param('path')));
    
    print $cgi->header(), $cgi->start_html();                                                                                               


    # remove redundant current directory and parent directory entries                                                                       
    my $pt = Path::Trim->new();                                                                                                             
    $pt->set_directory_separator('/');                                                                                                      
    $path = $pt->trim_path($path);                                                                                                          

    # remove all ../ and ./ that have accumulated at the beginning of the path and                                                            
    # make the path absolute by prepending a /                                                                                                

    $path =~ s{^ (\.\.? /)+ }{}x;                                                                                                           
    $path =~ s{(\.\.)+$}{}x;                                                                                                           
    #$path = "/$path" unless $path =~ m{^ / }x;                                                                                              

    #print $cgi->h1($path);                                                                                                                  

    opendir my $dh, $doc_root . "/" . $path or die $!;                                                                                                        
    my @entries = grep { $_ !~ /^ \. $/x } readdir $dh;
    closedir $dh;

    print $cgi->start_ol();
    for my $entry ( sort { $a cmp $b } @entries ) {

        next if ($entry eq "..");
        next if ($entry eq ".gitignore");
        next if ($entry eq ".doc");
        next if ($entry eq ".ftpquota");
        next if ($entry =~ /.cgi/);
        next if ($entry =~ /.ico/);
        next if ($entry =~ /.jpg/);

        #printf STDERR "path: %s/%s\n", $doc_root, $path;

        if ( -d File::Spec->catfile( $doc_root . "/" .$path, $entry ) ) {
            my $abs_entry = File::Spec->catfile( $doc_root . "/" . $path, $entry );
            my $anchor = $cgi->a( { 'href' => "?path=$path/$entry" }, $entry );
            print $cgi->li($anchor);
            next;
        }

        my $abs_entry = File::Spec->catfile( $doc_root . "/" . $path, $entry );
        #my $anchor = $cgi->a( { 'href' => "?file=$abs_entry" }, $entry );
        my $file = $entry;
        $file =~ s/.html//;
        $file =~ s/-$//;
        printf STDERR "path: %s\n", $path;
        my $anchor = $cgi->a( { 'href' => "$path/$entry", 'target' => 'top' }, $file );
        print $cgi->li($anchor);
        
    }
    print $cgi->br;

    for my $entry ( sort { $a cmp $b } @entries ) {

        printf STDERR "path2: %s\n", $entry;
        next unless ($entry =~ /.jpg$/);
        printf STDERR "path3: %s\n", $entry;


        my $abs_entry = File::Spec->catfile( $doc_root . "/" . $path, $entry );
        print $cgi->img( { 'src' => "$path/$entry", 'width' => 800 });
        
    }
    print $cgi->end_ol(), $cgi->end_html();
}
